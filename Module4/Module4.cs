﻿using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            Module4 module4 = new Module4();
            module4.Task_1_A(null);
        }


        public int Task_1_A(int[] array)
        {
            ChekArrayIsNull(array);
            return GetMaxElement(array);
        }

        public int Task_1_B(int[] array)
        {
            ChekArrayIsNull(array);
            return GetMinElement(array);
        }

        public int Task_1_C(int[] array)
        {
            ChekArrayIsNull(array);
            return GetSumElment(array);
        }

        public int Task_1_D(int[] array)
        {
            ChekArrayIsNull(array);
            return GetDifferenceOfElment(array);
        }

        public void Task_1_E(int[] array)
        {
            ChekArrayIsNull(array);
            IncreaseArrayElements(ref array);
        }
        private int GetMaxElement(int[] array)
        {
            return array.Max();
        }
        private int GetMinElement(int[] array)
        {
            return array.Min();
        }
        private int GetSumElment(int[] array)
        {
            return array.Sum();
        }
        private void IncreaseArrayElements(ref int[] array)
        {
            int maxElement = GetMaxElement(array);
            int minElement = GetMinElement(array);
            for (int i = 0; i < array.Length; i++)
            {
                if (i % 2 == 0)
                {
                    array[i] += maxElement;
                }
                else
                {
                    array[i] -= minElement;
                }
            }
        }
        private void ChekArrayIsNull(int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException($"Value cannot be null.Parameter name: {array}");
            }
            if (array.Length == 0)
            {
                throw new ArgumentNullException($"Value cannot be null.Parameter name: {array}");
            }
        }
        private int GetDifferenceOfElment(int[] array)
        {
            return GetMaxElement(array) - GetMinElement(array);
        }

        public int Task_2(int a, int b, int c)
        {
            return GetSumThreeNumbers(a, b, c);
        }
        private int GetSumThreeNumbers(int firstNumber, int secondNumber, int thirdNumber)
        {
            return firstNumber + secondNumber + thirdNumber;
        }
        private int GetSumTwoNumbers(int firstNumber, int secondNumber)
        {
            return firstNumber + secondNumber;
        }
        private double GetSumThreeDoubleNumbers(double firstNumber, double secondNumber, double thirdNumber)
        {
            return firstNumber + secondNumber + thirdNumber;
        }
        private string GetSumString(string firstRow, string secondRow)
        {
            return firstRow + secondRow;
        }

        public int Task_2(int a, int b)
        {
            return GetSumTwoNumbers(a, b);
        }

        public double Task_2(double a, double b, double c)
        {
            return GetSumThreeDoubleNumbers(a, b, c);
        }

        public string Task_2(string a, string b)
        {
            return GetSumString(a, b);
        }
        private int[] GetSumOfArray(int[] firstArray, int[] secondArray)
        {
            if(CheckTheSameLength(firstArray, secondArray))
            {
                for(int i = 0; i < firstArray.Length; i++)
                {
                    firstArray[i] += secondArray[i];
                }
                return firstArray;
            }
            else
            {
                for(int i = 0; i < firstArray.Length; i++)
                {
                    if(i == firstArray.Length || i == secondArray.Length)
                    {
                        return firstArray;
                    }
                    secondArray[i] += firstArray[i];
                }
                return secondArray;
            }
        }
        private bool CheckTheSameLength(int[] firstArray, int[] secondArray)
        {
            if (firstArray.Length == secondArray.Length)
            {
                return true;
            }
            return false;
        }
        private void ChekArrayIsNull(int[] array, int[] secondArray)
        {
            if (array == null || secondArray == null)
            {
                throw new ArgumentNullException($"Value cannot be null.Parameter name: {array}, {secondArray}");
            }
            if (array.Length == 0 || secondArray.Length == 0)
            {
                throw new ArgumentNullException($"Value cannot be null.Parameter name: {array}, {secondArray}");
            }
        }
        public int[] Task_2(int[] a, int[] b)
        {
            ChekArrayIsNull(a, b);
            return GetSumOfArray(a, b);
        }
        private void GetValueIncrease(ref int firstNumber, ref int secondNumber, ref int thirdNumber)
        {
            firstNumber += 10;
            secondNumber += 10;
            thirdNumber += 10;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            GetValueIncrease(ref a,ref b, ref c);
        }

        private void GetLegthCircle(double radius, out double length)
        {
            CheckIsPositiveRadius(radius);
            length = 2 * Math.PI * radius;
        }
        private void GetSquareCircle(double radius, out double square)
        {
            CheckIsPositiveRadius(radius);
            square = Math.PI * Math.Pow(radius,2);
        }
        private void CheckIsPositiveRadius(double radius)
        {
            if(radius < 0)
            {
                throw new ArgumentException("radius must be positive");
            }
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            GetLegthCircle(radius, out length);
            GetSquareCircle(radius, out square);
        }
        private void GetMaxElement(int[] array, out int maxItem)
        {
            ChekArrayIsNull(array);
            maxItem = array.Max();
        }
        private void GetMinElement(int[] array, out int minItem)
        {
            ChekArrayIsNull(array);
            minItem = array.Min();
        }
        private void GetSumElment(int[] array, out int sumOfItem)
        {
              ChekArrayIsNull(array);
              sumOfItem = array.Sum();
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            GetMaxElement(array, out maxItem);
            GetMinElement(array, out minItem);
            GetSumElment(array, out sumOfItems);
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
           return GetValueIncrease(numbers);
        }
        private (int,int, int) GetValueIncrease ((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;
            return numbers;
        }

        public (double, double) Task_4_B(double radius)
        {
           return GetLegthCircle(radius);
        }
        private (double, double) GetLegthCircle(double radius)
        {
            CheckIsPositiveRadius(radius);
            (double, double) tupleResult;
            tupleResult.Item1 = 2 * Math.PI * radius;
            tupleResult.Item2 = GetSquareCircle(radius);
            return tupleResult;
        }
        private double GetSquareCircle(double radius)
        {
            return Math.PI * Math.Pow(radius, 2);
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            return GetTuple(array);
        }
        private (int,int,int) GetTuple(int[] array)
        {
            ChekArrayIsNull(array);
            (int, int, int) resultTuple;
            resultTuple.Item1 = GetMinElement(array);
            resultTuple.Item2 = GetMaxElement(array);
            resultTuple.Item3 = GetSumElment(array);
            return resultTuple;
        }

        public void Task_5(int[] array)
        {
            IncreaseElement(ref array);
        }
        private void IncreaseElement(ref int[] array)
        {
            ChekArrayIsNull(array);
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += 5;
            }
        } 

        public void Task_6(int[] array, SortDirection direction)
        {
            switch (direction)
            {
                case SortDirection.Ascending:
                    ChekArrayIsNull(array);
                    SortAsseding(ref array);
                    break;
                case SortDirection.Descending:
                    ChekArrayIsNull(array);
                    SortDescending(ref array);
                    break;
            }
        }
        private void SortAsseding(ref int[] array)
        {
            Array.Sort(array);
        }
        private void SortDescending(ref int[] array)
        {
            Array.Sort(array);
            Array.Reverse(array);
        }


        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            result = GetStartLine(func, x1, x2, e);
            return result;
        }

        private double GetStartLine(Func<double, double> func, double x1, double x2, double e)
        {
            double exactSolution = (x1 + x2) / 2;
            if (func(x1) * func(exactSolution) < 0)
            {
                x2 = exactSolution;
            }
            else
            {
                x1 = exactSolution;
            }
            if(Math.Abs(x2-x1) > 2* e)
            {
                GetStartLine(func, x1, x2, e);
            }
            else
            {
                exactSolution = (x1 + x2) / 2;
            }
            return exactSolution;
        }
    }
}
